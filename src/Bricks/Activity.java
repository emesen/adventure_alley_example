package Bricks;

import java.util.ArrayList;


public class Activity
{

    private int ID;
    private String name;
    private String instructor;
    private String startTime;
    private String endTime;
    private int[] days;
    private ArrayList<Restriction> restrictionList;
    
    
   public Activity (int ID, String name, String instructor,
            String startTime, String endTime, int[] days, ArrayList<Restriction> list)
    {
        this.ID = ID;
        this.name = name;
        this.instructor = instructor;
        this.startTime = startTime;
        this.endTime = endTime;
        this.days = days;
        this.restrictionList = list;
        
    }

    public ArrayList<Restriction> getRestrictionList() {
        return restrictionList;
    }

    public void setList(ArrayList<Restriction> list) {
        this.restrictionList = list;
    }
   

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }


    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getInstructor()
    {
        return instructor;
    }

    public void setInstructor(String instructor)
    {
        this.instructor = instructor;
    }

    public String getStartTime()
    {
        return startTime;
    }

    public void setStartTime(String startTime)
    {
        this.startTime = startTime;
    }

    public String getEndTime()
    {
        return endTime;
    }

    public void setEndTime(String endTime)
    {
        this.endTime = endTime;
    }

    public int[] getDays()
    {
        return days;
    }

    public void setDays(int[] days)
    {
        this.days = days;
    }


}