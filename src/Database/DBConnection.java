package Database;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Pelle
 */
public class DBConnection {
  
    final static String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    final static String DB_URL = "jdbc:mysql://localhost/adventure_alley";
    final static String username = "root";
    final static String password = "root";
    
    public static Connection getConnection() {

        Connection connection;
        try {
            Class.forName(DBConnection.JDBC_DRIVER); //This code is forcing the code to load and initialize
            
            /*  Attemps to establish a connection to the given database URL. The DriverManager attempts to 
                select an appropiate driver from the set of registered JDBC drivers
            */
            connection = DriverManager.getConnection(DBConnection.DB_URL, DBConnection.username, DBConnection.password);

            return connection;

        } catch (ClassNotFoundException | SQLException e) { // If no connection made, then it catch an ClassNotFoundException or SQLException.
            System.err.println(e);
            return null;
        }
       
    }
}
